#! /bin/bash

# ZTE Rooting utility, for Linux and Cygwin because I hate working with Windows
# Batch Files.

Linux_Root () {
	`echo "sudo adb kill-server"`
	if [ -e root/zergRush ]; then
		`echo "sudo adb wait-for-device"`; `echo "sudo adb push root/zergRush /data/local/"`
		`echo "sudo adb shell chmod 777 /data/local/zergRush"`; `echo "sudo adb shell rm /data/local/tmp/*sh"`
		`echo "sudo adb shell /data/local/zergRush"`; `echo "sudo adb wait-for-device"`; `echo "sudo adb shell sleep 2"`
		`echo "sudo adb remount"`; `echo "sudo adb shell mount -o rw,remount rootfs /"`; 
		`echo "sudo adb shell mount -o remount,suid /dev/block/mmcblk0p29 /system"`; 
		`echo "sudo adb shell chmod 4755 /system/bin/sh"`; `echo "sudo adb push system/xbin/su /system/xbin/"`
		`echo "sudo adb push system/xbin/busybox /system/xbin/"`; `echo "sudo adb shell chmod 4755 /system/xbin"`
		`echo "sudo adb shell chmod 4755 /system/xbin/su"`; `echo "sudo adb shell chmod 4755 /system/xbin/busybox"`
		`echo "sudo adb push system/app/Superuser.apk /system/app/"`
		`echo "sudo adb push system/app/androidterm.apk system/app/"`; `echo "sudo adb wait-for-device"`
		`echo "sudo adb shell sync"`
		`echo "sudo adb reboot"`
	fi
}

Cygwin_Root () {
	if [ -e root/zergRush ]; then
		`echo "adb wait-for-device"`; `echo "adb push root/zergRush /data/local/"`; `echo "adb shell chmod 777 /data/local/zergRush"`;
		`echo "adb shell rm /data/local/tmp/*sh"`; `echo "adb shell /data/local/zergRush"`;
		`echo "adb wait-for-device"`; `echo "adb shell sleep 2"`; `echo "adb remount"`
		`echo "adb shell mount -o rw,remount rootfs /"`; `echo "adb shell mount -o remount,suid /dev/block/mmcblk0p29 /system"`
		`echo "adb shell chmod 4755 /system/bin/sh"`; `echo "adb push system/xbin/su /system/xbin/"`
		`echo "adb push system/xbin/busybox /system/xbin/"`; `echo "adb shell chmod 4755 /system/xbin"`
		`echo "adb shell chmod 4755 /system/xbin/su"`; `echo "adb shell chmod 4755 /system/xbin/busybox"`
		`echo "adb push system/app/Superuser.apk /system/app/"`; `echo "adb push system/app/androidterm.apk system/app/"`
		`echo "adb wait-for-device"`; `echo "adb shell sync"`
		`echo "adb reboot"`
	fi
}

if [ `uname | grep CYGWIN` ]; then
	Cygwin_Root
elif [ `uname | grep LINUX` ]; then
	Linux_Root
fi
